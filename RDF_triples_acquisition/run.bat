rem create an empty output file
echo. 2>output.ttl

rem Run the docker container in debug mode
docker run --rm -it --net protrait-fairifier_default ^
    --link protrait-fairifier_postgres_1:postgresdb ^
    -e DB_JDBC=jdbc:postgresql://172.17.0.1:5432/mydata ^
    -e DB_USER=postgres ^
    -e DB_PASS=postgres ^
    -v "%cd%\mapping.ttl":/mapping.ttl ^
    -v "%cd%\output.ttl":/output.ttl ^
    registry.gitlab.com/um-cds/fair/tools/r2rml:1.0 bash run.sh --debug