# create an empty output file
echo >output.ttl

# Run the docker container in debug mode
docker run --rm -it \
    --link postgresdb:postgresdb \
    -e DB_JDBC=jdbc:postgresql://postgresdb:5432/bms \
    -e DB_USER=postgres \
    -e DB_PASS=postgres \
    -v $(pwd)/mapping.ttl:/mapping.ttl \
    -v $(pwd)/output.ttl:/output.ttl \
    registry.gitlab.com/um-cds/fair/tools/r2rml:1.0 bash run.sh --debug

#-v "%cd%\data":/data ^
#-e DB_JDBC=jdbc:relique:csv:/data ^

#--link postgresdb:postgresdb ^
#-e DB_JDBC=jdbc:postgresql://postgresdb:5432/bms ^