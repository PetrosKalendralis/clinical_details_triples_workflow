RDF triples of clinical data of the collections 


1.https://wiki.cancerimagingarchive.net/display/Public/NSCLC-Radiomics (NSCLC-Radiomics)


2.https://wiki.cancerimagingarchive.net/display/Public/Head-Neck-Radiomics-HN1 (Head-Neck-Radiomics-HN1)


3.https://wiki.cancerimagingarchive.net/display/Public/NSCLC-Radiomics-Interobserver1 (NSCLC-Radiomics-Interobserver1)



Following the steps of the repository https://github.com/maastroclinic/DataFAIRifier the .CSV files of the clinical details of the datasets were mapped with the R2RML language and imported into SQL databases.


To acquire the output .ttl files you should run the run.bat from the directory "RDF_triples_acquisition" after creating your R2RML mapping to the mapping.ttl file.

